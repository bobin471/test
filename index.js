class MyBigNumber {
  static sum(stn1, stn2) {
    let result = "";
    let carry = 0; // số dư khi cộng 2 số

    // Duyệt chuỗi từ phải sang trái
    for (
      let i = stn1.length - 1, j = stn2.length - 1;
      i >= 0 || j >= 0;
      i--, j--
    ) {
      const digit1 = i < 0 ? 0 : parseInt(stn1[i], 10); // nếu i < 0 thì lấy số 0, ngược lại lấy số từ chuỗi stn1
      const digit2 = j < 0 ? 0 : parseInt(stn2[j], 10); // nếu j < 0 thì lấy số 0, ngược lại lấy số từ chuỗi stn2

      const sum = digit1 + digit2 + carry; // tổng 2 số và số dư
      const digitSum = sum % 10; // lấy kết quả 1 chữ số của tổng
      carry = sum >= 10 ? 1 : 0; // tính số dư khi tổng >= 10

      result = digitSum + result; // chèn kết quả 1 chữ số vào đầu chuỗi kết quả
    }

    // Nếu số dư cuối cùng khác 0, ta sẽ thêm số dư đó vào kết quả
    if (carry > 0) {
      result = carry + result;
    }

    return result;
  }
}

console.log(MyBigNumber.sum("123", "456")); // kết quả là "579"
console.log(MyBigNumber.sum("999", "1")); // kết quả là "1000"
console.log(MyBigNumber.sum("999", "999")); // kết quả là "1998"
console.log(MyBigNumber.sum("123456789", "987654321")); // kết quả là "1111111110"
